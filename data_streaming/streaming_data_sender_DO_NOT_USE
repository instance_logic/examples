-----------------------------------------------------------------------------------------------------------------------
-- parallel data streaming with up-stream hold-off
-- This is the fundamental concept used by well-known protocols such as ARM’s AMBA AXI4 streaming
-- for transferring frames of data from one node in a system to another
--
--------------------
-- interface notes:
-- upst_tvalid is asserted from upstream to indicate when upst_tdata is valid
-- upst_tlast is asserted upstream to indicate when upst_tdata is the least beat of a transfer
-- upst_tdata contains the parallel streaming data bytes
-- upst_tkeep indicates which upstream data bytes contain data
-- upst_tready indicates this unit has consumed the value on upst_tdata and is ready for the next data
--
----------------
-- description:
-- Whilst this design unit alone is of limited use, the important point is the input interface mechanism.
--
-- The input interface uses a pull mechanism where the upstream node can assert its data valid at any time, 
-- but is not allowed to update its output for the next data until this node has asserted 'tready'
-- to indicate it has consumed the current data. This is a very convenient mechanism for data streeaming and 
-- pipelining because wait-state handling becomes trivial from the point of view of the upstream node - simply
-- do not update the outputs until the 'downst_tready' input has been asserted.
--
-- In normal circumstances, a unit receiving data from this type of interface would process the data in some
-- way, then forward the data and / or some processing result on to a downstream unit via the same type of
-- interface. The processing may consume an unknown number of clock cycles, which highlights the need for
-- the upstream 'tready' signal to hold off new data until it can be processed.
-- 
-- The tkeep signal indicates how many of the input bytes contain data. This is useful for when the number
-- of bytes to be transferred is not an integer multiple of the width (in bytes) of the input data.
-- The tlast signal indicates when the current input data is the last, and is used in common scenarios where
-- a frame of data is being transferred.
--
---------------------
-- More information:
-- For detailed information on AMBA AXI4 streaming, please download the specification from
-- http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ihi0051a/index.html
-----------------------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std_unsigned.all;

entity streaming_data_sender is
    generic(
        K_DATA_WIDTH_IN_BYTES : integer := 8    -- width in bytes of input data
    );
    port (
        clk : in std_logic;
        rst : in std_logic;
        
        -- upstream interface (data input stream with hold-off)
        downst_tvalid : out std_logic; -- output data valid
        downst_tlast : out std_logic;  -- indicates last beat of output data
        downst_tdata : out std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);   -- input data
        downst_tkeep : out std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0); -- indicates which input bytes contain data
        downst_tready : in std_logic    -- output indicates this unit is ready to receive data
        
        -- **NOTES:
        --        - the 'upst' prefix is short for 'upstream'
        --        - the 't' prefix is standard in the AXI4 streaming protocol    
    );
end entity streaming_data_sender;

architecture RTL of streaming_data_sender is
    

    
begin


end architecture RTL;
