library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std_unsigned.all;
use std.textio.all;

library osvvm;
use osvvm.RandomPkg.all;

library util;
use util.tb_util_pkg.all;

package data_streaming_tb_pkg is

	procedure p_send_frame_data (
	    signal clk : in std_logic;
	    frame_data : t_byte_array;

	    signal tvalid : out std_logic;
	    signal tlast : out std_logic;
	    signal tdata : out std_logic_vector;	-- width must be a multiple of 8 bits
	    signal tkeep : out std_logic_vector;	-- width must be tdata width / 8
	    signal tready : in std_logic;

	    tvalid_gap_out_of_ten : integer range 0 to 9;
	    ready_timeout : time;
	    constant K_TIMEOUT_MESSAGE : string := "p_send_frame_data: UUT spent too long not ready (tready = '0')"
	);

	-- listens to axi interface and stores data to t_byte_array
	procedure p_get_frame_data(
	    signal clk : in std_logic;
	    signal tvalid : in std_logic;
	    signal tdata : in std_logic_vector;
	    signal tkeep : in std_logic_vector;
	    signal tlast : in std_logic;
	    signal tready : out std_logic;
	    constant K_TIMEOUT : time := 10_000 ms;
	    constant K_TIMEOUT_MESSAGE : string := "timed out";
	    constant K_MESSAGE_SEVERITY : severity_level;
	    frame_data : out t_byte_array
	);
	
end package data_streaming_tb_pkg;

-----------------------------------------------------------------------------------------------------------------------
-- package body
package body data_streaming_tb_pkg is

	function calculate_tkeep(remaining_bytes : natural; constant K_DATA_WIDTH_IN_BYTES : natural) return std_logic_vector is
    	variable tkeep : std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0);
	begin
	    -- appropriate bits set if remaining bytes less than byte width
	    if (remaining_bytes < K_DATA_WIDTH_IN_BYTES) then
	        tkeep := (others => '0');
	        for I in 0 to K_DATA_WIDTH_IN_BYTES-1 loop
	            if (I < remaining_bytes) then
	                tkeep(I) := '1';
	            end if;
	        end loop;
	    -- all 1s if remaining bytes more than byte width
	    else
	        tkeep := (others => '1');
	    end if;

	    return tkeep;
	end function calculate_tkeep;

--------------------------------------------
-- send frame data over streaming interface
	procedure p_send_frame_data (
	    signal clk : in std_logic;
	    frame_data : t_byte_array;

	    signal tvalid : out std_logic;
	    signal tlast : out std_logic;
	    signal tdata : out std_logic_vector;
	    signal tkeep : out std_logic_vector;
	    signal tready : in std_logic;

	    tvalid_gap_out_of_ten : integer range 0 to 9;
	    ready_timeout : time;
	    constant K_TIMEOUT_MESSAGE : string := "p_send_frame_data: UUT spent too long not ready (tready = '0')"
	) is

	    constant K_tdata_WIDTH_IN_BYTES : integer := tdata'length / 8;
	    constant K_RAWDATA_LENGTH_IN_BYTES : integer := frame_data'length;

	    variable byte_count : integer;   -- used to index input data
	    variable remaining_bytes : integer;  -- used to set tkeep value
	    
	    -- to control wait states
	    variable rand_gen : osvvm.RandomPkg.RandomPType;
	    variable wait_state : boolean;

	begin

	    -- initialise
	    tvalid <= '0';
	    tlast  <= '0';
	    tdata <= to_stdlogicvector(0, tdata'length);
	    tkeep <= to_stdlogicvector(0, tkeep'length);	
	    byte_count := 0;
	    rand_gen.initSeed(0);

	    -- send all bytes
	    while (byte_count < K_RAWDATA_LENGTH_IN_BYTES) loop

	        -- calculate remaining bytes
	        remaining_bytes := K_RAWDATA_LENGTH_IN_BYTES - byte_count;
	        
	        -- determine whether to execute a wait state or not
	        if (rand_gen.RandInt(0, 9) < tvalid_gap_out_of_ten) then
	        	wait_state := TRUE;
	        else
	        	wait_state := FALSE;
	        end if;

	        -- either send a word or execute a wait state
	        if (not(wait_state)) then
		        -- assert DV
		        tvalid <= '1';

		        -- assert tkeep bits according to how many bytes are left
		        tkeep <= calculate_tkeep(remaining_bytes, K_tdata_WIDTH_IN_BYTES);

		        -- output data
		        tdata(tdata'range) <= (others => '0');	-- zeroize unused
		        for I in 0 to K_TDATA_WIDTH_IN_BYTES-1 loop
		            if (I < remaining_bytes) then    -- don't go off the end
		                tdata((I+1)*8-1 downto I*8) <= frame_data(byte_count+I);
		            end if;
		        end loop;

		        -- set tlast if last word
		        if (remaining_bytes <= K_tdata_WIDTH_IN_BYTES) then
		            tlast <= '1';
		        end if;

		        -- wait for axi interface to consume data
		        p_wait_for_1(
	                    clk => clk, 
	                    input_bits(0) => tready, 
	                    K_TIMEOUT => ready_timeout, 
	                    timeout_message => K_TIMEOUT_MESSAGE, 
	                    message_severity => failure
	                    );

		        -- increment counters
		        remaining_bytes := K_RAWDATA_LENGTH_IN_BYTES - byte_count;
		        byte_count := byte_count + K_tdata_WIDTH_IN_BYTES;
		    
		    -- wait state
		    else
		    	tvalid <= '0';
		    	tdata(tdata'range) <= (others => 'X');
		    	tkeep(tkeep'range) <= (others => 'X');
		    	wait until rising_edge(clk);
		    end if;

	    end loop;

	    -- zeroize interface
	    tvalid <= '0';
	    tlast  <= '0';
	    tdata <= to_stdlogicvector(0, tdata'length);
	    tkeep <= to_stdlogicvector(0, tkeep'length);	

	end p_send_frame_data;

-------------------------------------------
-- get frame data from streaming interface
	procedure p_get_frame_data(
	    signal clk : in std_logic;
	    signal tvalid : in std_logic;
	    signal tdata : in std_logic_vector;
	    signal tkeep : in std_logic_vector;
	    signal tlast : in std_logic;
	    signal tready : out std_logic;
	    constant K_TIMEOUT : time := 10_000 ms;
	    constant K_TIMEOUT_MESSAGE : string := "timed out";
	    constant K_MESSAGE_SEVERITY : severity_level;
	    frame_data : out t_byte_array
	) is

		variable data_index : integer;
		variable frame_data_v : t_byte_array(frame_data'range);

	begin

	    -- initialize
	    frame_data(frame_data'range) := (others => X"00");
	    data_index := 0;
	    tready <= '1';	-- TODO: valid gaps

	    -- wait for first data
	    p_wait_for_1(
	    	clk => clk, 
	    	input_bits(0) => tvalid, 
	    	K_TIMEOUT => K_TIMEOUT, 
	    	timeout_message => K_TIMEOUT_MESSAGE,
	    	message_severity => K_MESSAGE_SEVERITY
	    	);

	    -- keep filling up data until eop
	    while (tlast /= '1') loop

	        -- load valid bytes into store
	        if ((tvalid = '1') and (tready = '1')) then
	            p_store_valid_bytes(tkeep, tdata, data_index, frame_data_v);
	            data_index := data_index + get_number_of_1s(tkeep);
	        end if;

	        -- update output signal
	        frame_data := frame_data_v;

	        wait until rising_edge(clk);

	    end loop;

	    -- load last data (in case EoP is '1' on first or last clock of DV)
	    while (tready /= '1') loop -- loop is in case of wait states
	        wait until rising_edge(clk);
	    end loop;
	    
	    -- ready on last means done 
	    p_store_valid_bytes(tkeep, tdata, data_index, frame_data_v);
	    wait until rising_edge(clk);
	    
	    -- update the output signal
	    frame_data := frame_data_v;

	    -- zeroize tready
	    tready <= '0';

	end procedure p_get_frame_data;
	
end package body;
