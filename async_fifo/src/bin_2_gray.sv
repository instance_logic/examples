

module bin_2_gray 
#(parameter int WIDTH=8)
(
    input logic[WIDTH-1:0] binary_in,
    output logic[WIDTH-1:0] gray_out    
);

always_comb gray_out[WIDTH-1:0] <= binary_in[WIDTH-1:0] ^ {1'b0, binary_in[WIDTH-1:1]};

endmodule // bin_2_gray