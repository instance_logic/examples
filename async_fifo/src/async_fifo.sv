//======================================================
// asynchronous fifo for crossing clock domains
//------------------------------------------------------

module async_fifo 
#(parameter int DATA_WIDTH=8, ADDRESS_WIDTH=8)
(
    // write interface
    input logic write_clk,
    input logic write_enable,
    input logic[DATA_WIDTH-1:0] write_data,
    output logic write_full,

    // read interface
    
    
);

endmodule