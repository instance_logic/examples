//=============================================================
// dual port RAM
// infers technology-specific true dual-port block RAM
//-------------------------------------------------------------
`ifndef RAM_DP_RW_INFERRED_SV
`define RAM_DP_RW_INFERRED_SV

module ram_dp_rw_inferred
#(parameter 
    int DATA_WIDTH=1, 
    int ADDRESS_WIDTH=14,
    string RAM_STYLE="M20K"
    //**TODO: parameter for registered output
    )
(
    // write interface 
    input logic write_clk,
    input logic[DATA_WIDTH-1:0] write_data, 
    input logic write_enable,
    input logic[ADDRESS_WIDTH-1:0] write_address,

    // read interface 
    input logic read_clk,
    output logic[DATA_WIDTH-1:0] read_data,
    input logic[ADDRESS_WIDTH-1:0] read_address
);

localparam RAM_DEPTH = 1 << ADDRESS_WIDTH;

// memory bank
(* ramstyle = RAM_STYLE *) logic[DATA_WIDTH-1:0] ram[RAM_DEPTH-1:0];

// write port
always_ff @(posedge write_clk) begin
    if (write_enable)
        ram[write_address] <= write_data;
end

// read port
//always_ff @(posedge read_clk) begin
always_comb
    read_data <= ram[read_address];
//end

endmodule

`endif