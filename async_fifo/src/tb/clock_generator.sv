module clock_generator
#(parameter time CLK_PERIOD=10ns)
(
    output bit clk
);

    initial clk = 0;
    always #(CLK_PERIOD/2) clk = ~clk;

endmodule