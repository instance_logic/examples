
`ifndef FIFO_INTERFACES_SV
`define FIFO_INTERFACES_SV

interface fifo_write_if #(parameter int DATA_WIDTH=8) (input clk);

    logic write_enable;
    logic write_full;
    logic write_data[DATA_WIDTH-1:0];

    modport fifo_uut(input clk, write_enable, write_data, output write_full);
    modport fifo_monitor(input clk, write_enable, write_full, write_data);
    modport fifo_driver(input clk, write_full, output write_enable, write_data);

endinterface

interface fifo_read_if #(parameter int DATA_WIDTH=8) (input clk);

    logic read_enable;
    logic read_empty;
    logic read_data[DATA_WIDTH-1:0];

    modport fifo_uut(input clk, read_enable, output read_data, read_empty);
    modport fifo_monitor(input clk, read_enable, read_empty, read_data);
    modport fifo_driver(input clk, read_empty, output read_enable, read_data);

endinterface

interface single_reset;
    logic rst_n;

    modport uut(input rst_n);
    modport tb(output rst_n);
    
endinterface

`endif