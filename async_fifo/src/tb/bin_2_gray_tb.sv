
module bin_2_gray_tb ();
localparam COUNT_WIDTH=4;

bit clk;

logic[COUNT_WIDTH-1:0] counter, gray_count;

initial begin
    clk = 1'b0;
    counter = 0;
end

always #5 clk = ~clk;

always @(posedge clk)
    counter <= counter + 1;

bin_2_gray 
#(.WIDTH(COUNT_WIDTH))
UUT (
    .binary_in(counter),
    .gray_out(gray_count)    
);

endmodule // bin_2_gray_tb