`ifndef FIFO_TB_PKG_SV
`define FIFO_TB_PKG_SV

`include "clock_generator.sv"

package fifo_tb_pkg;

	parameter integer DATA_WIDTH = 8;
	parameter integer ADDRESS_WIDTH = 5;
	parameter integer NUM_RAND_TRANSACTIONS = 16;

	parameter time RESET_DURATION = 30ns;

	parameter time WRITE_CLK_PERIOD = 6.67ns;
    parameter time READ_CLK_PERIOD = 5ns; 

	class fifo_scoreboard;

		mailbox fifo_model = new();
		integer item_count;

		function new();
			item_count = 0;
		endfunction

		task add_item(input logic[DATA_WIDTH-1:0] write_data, output int error_status);
			
			error_status = 0;

			// detect overflow
			if (item_count == (2**DATA_WIDTH)-1) begin
				$display("%0dns add_item: overflow - nothing added!", $time);
				error_status = -1;
	// todo: detect bogus full
			// add item
			end else begin
				fifo_model.put(write_data);
				item_count++;
			end

		endtask

		task check_item(input logic[DATA_WIDTH-1:0] read_data, output int error_status);
			
			logic[DATA_WIDTH-1:0] compare_data;

			error_status = 0;

			// detect underflow
			if (item_count == 0) begin
				$display("underflow - nothing compared!");
				error_status = -1;
	// todo: detect bogus empty		
			// pop item and check against read data	
			end else begin
				// get
				fifo_model.get(compare_data);
				// check
				if (read_data != compare_data) begin
					$display("%0dns check_item: incorrect data - expected 0x%0h read 0x%0h\n", $time, compare_data, read_data);
					error_status = -1;
				end
				// count removed item
				item_count--;
			end
		endtask

	endclass : fifo_scoreboard

	class fifo_monitor;

		virtual fifo_write_if #(.DATA_WIDTH(DATA_WIDTH)).fifo_monitor write_if;
		virtual fifo_read_if #(.DATA_WIDTH(DATA_WIDTH)).fifo_monitor read_if;

		function new (
          virtual fifo_write_if.fifo_monitor push_interface,
          virtual fifo_read_if.fifo_monitor pop_interface
			);

			this.write_if = push_interface;
			this.read_if = pop_interface;

		endfunction

		task go(ref fifo_scoreboard scoreboard, output int read_error, write_error);
			// start read and write monitors
			fork
				monitor_reads(scoreboard, read_error);
				monitor_writes(scoreboard, write_error);
			join_none
		endtask

		task monitor_writes(ref fifo_scoreboard scoreboard, output int error_status);
			logic[DATA_WIDTH-1:0] captured_data;

			forever begin
				
				// detect writes and add to scoreboard
				@(posedge write_if.clk);
				if (write_if.write_enable) begin
					// check in case fifo was full
					if (write_if.write_full)
						$error("fifo_monitor.monitor_writes: ERROR - detected write during full!");

					// add data to scoreboard
					captured_data = write_if.write_data;
					scoreboard.add_item(captured_data, error_status);
				end

			end
		endtask

		task monitor_reads(ref fifo_scoreboard scoreboard, output int error_status);
			logic[DATA_WIDTH-1:0] captured_data;

			forever begin

				// detect reads and compare in scoreboard
				@(posedge read_if.clk);
				if (read_if.read_enable) begin
					// check in case fifo was empty
					if (read_if.read_full)
						$error("fifo_monitor.monitor_reads: ERROR - detected read during empty!");

					// compare data in scoreboard
					captured_data = read_if.read_data;
					scoreboard.check_item(captured_data, error_status);
				end

			end
		endtask

	endclass

	class fifo_driver;

		virtual fifo_write_if #(.DATA_WIDTH(DATA_WIDTH)).fifo_driver write_if;
		integer num_writes;

		function new(
			virtual fifo_write_if.fifo_driver push_interface,
			integer num_writes_todo
			);

			this.write_if = push_interface;
			this.num_writes = num_writes_todo;

		endfunction

		task write_single_data(logic[DATA_WIDTH-1:0] data_to_write);
			@(posedge write_if.clk);
			write_if.write_enable = 1'b1;
			write_if.write_data = data_to_write;
		endtask

		task write_all_data();
			logic[DATA_WIDTH-1:0] data_to_write;

			// start on a clock edge
			@(posedge write_if.clk);
			for (int i = 0; i < num_writes; i++) begin
				// set random data
				data_to_write = $urandom_range(0,2**DATA_WIDTH-1);
				// make sure not full
				while (write_if.write_full != 1'b0) begin
					write_if.write_enable = 1'b0;
					@(posedge write_if.clk);
	//todo: timeout!
				end
				// send data
				write_single_data(data_to_write);
			end
			// clear control signals
			@(posedge write_if.clk);
			write_if.write_enable = 1'b0;

		endtask

	endclass

	class resetter;
		
		virtual single_reset rst_if;

		// instance identifier
		static integer instances = 0;
		integer instance_id;

		localparam DEFAULT_MSG_PREFIX = "    reset generator";
		string status_msg_prefix;

		// how long the reset lasts if no duration is given
		time default_duration;

		function new(
			virtual single_reset reset_if, 
			input time default_duration, 
			string status_msg_prefix = DEFAULT_MSG_PREFIX
			);

			this.default_duration = default_duration;
			instances++;
			this.instance_id = instances;
			this.status_msg_prefix = status_msg_prefix;
			this.rst_if = reset_if;

			rst_if.tb.rst_n = 1'b1;

		endfunction

		task reset(time duration = default_duration);
			
			rst_if.tb.rst_n = 1'b0;
			$display("%s %d: UUT reset on at %f", status_msg_prefix, instance_id, $time);
			
			#duration;
			
			rst_if.tb.rst_n = 1'b1;
			$display("%s %d: UUT reset off at %f", status_msg_prefix, instance_id, $time);

		endtask
	endclass

endpackage

`endif