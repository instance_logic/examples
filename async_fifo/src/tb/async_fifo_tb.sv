`include "fifo_interfaces.sv"
`include "fifo_tb_pkg.sv"

program async_fifo_tb(fifo_write_if write_if, fifo_read_if read_if, single_reset rst_if); 

    import fifo_tb_pkg::*;

    // instantiate socreboard, drivers, monitors, etc.
    fifo_scoreboard scoreboard = new();
    fifo_monitor uut_monitor = new(write_if, read_if);
    fifo_driver uut_driver = new(write_if, NUM_RAND_TRANSACTIONS);
    resetter uut_resetter = new(rst_if, RESET_DURATION);
    
    initial begin
// reset test
        $display("executing reset_test...");

        // reset UUT 
        uut_resetter.reset();

        // check empty = 1
        a_reset_empty: assert (read_if.fifo_monitor.read_empty == 1'b1)
        else $error("reset_test FAIL: uut empty != 1 after reset - reset test FILED!");
        
        // check full = 0
        a_reset_full: assert (write_if.fifo_monitor.write_full == 1'b0)
        else $error("reset_test FAIL: uut full != 0 after reset - reset test FILED!");

        $display("reset test PASSED!");

// single read then write test
      
      $stop();
    end

endprogram

module top();
    import fifo_tb_pkg::*;

// UUT
    logic rst_n;

    bit write_clk;
    logic[DATA_WIDTH-1:0] write_data;
    logic write_enable;
    logic write_full;

    bit read_clk;
    logic[DATA_WIDTH-1:0] read_data;
    logic read_enable;
    logic read_empty;

    asynchronous_fifo#(DATA_WIDTH, ADDRESS_WIDTH, "MLAB")
    UUT (
      // write interface
      .write_clk(write_clk),
      .write_data(WRITE_IF.fifo_uut.write_data), 
      .write_enable(WRITE_IF.fifo_uut.write_enable),   // this gets blocked when fifo is full
      .write_full(WRITE_IF.fifo_uut.write_full),

      // read interface
      .read_clk(read_clk),
      .read_data(READ_IF.fifo_uut.read_data),
      .read_enable(READ_IF.fifo_uut.read_enable),   // this gets blocked when fifo is empty
      .read_empty(READ_IF.fifo_uut.read_empty),

      // async reset for both read and write interfaces
      .rst_n(RESETTER.uut.rst_n)
    );

// read and write interfaces
    fifo_write_if #(DATA_WIDTH) WRITE_IF (.clk(write_clk));
    fifo_read_if #(DATA_WIDTH) READ_IF (.clk(read_clk));

// clock generators
    clock_generator #(WRITE_CLK_PERIOD) WRITE_CLOCK_GENERATOR (write_clk);
    clock_generator #(READ_CLK_PERIOD) READ_CLOCK_GENERATOR (read_clk);

// reset interface
    single_reset RESET_IF();

// tb program
    async_fifo_tb tb_program(WRITE_IF, READ_IF, RESET_IF); 

endmodule