//=============================================================
// for synchronising multiple-bit signals across clock domains
// at high bandwidth on the slower clock
//-------------------------------------------------------------

`ifndef ASYNCHRONOUS_FIFO_SV
`define ASYNCHRONOUS_FIFO_SV

`include "fifo_pointer_gray.sv"
`include "ram_dp_rw_inferred.sv"

module asynchronous_fifo
#(parameter 
    int DATA_WIDTH=8, 
    int ADDRESS_WIDTH=5,    // must be 3 or more 
    string RAM_STYLE="M20K" // only "MLAB" and M20K tested so far
    )
(
    // write interface
    input logic write_clk,
    input logic [DATA_WIDTH-1:0] write_data, 
    input logic write_enable,   // this gets blocked when fifo is full
    output logic write_full,

    // read interface
    input logic read_clk,
    output logic[DATA_WIDTH-1:0] read_data,
    input logic read_enable,   // this gets blocked when fifo is empty
    output logic read_empty,

    // async reset for both read and write interfaces
    input logic rst_n
);

// write side logic (top bit of pointer is used to distinguish between full and empty)
logic[ADDRESS_WIDTH:0] write_pointer;
logic[ADDRESS_WIDTH:0] write_ptr_gray;
logic[ADDRESS_WIDTH:0] wsync_rp_gray0, wsync_rp_gray; // read pointer to write clock domain sync
logic s_write_enable;
logic full_flag;

// read side logic (top bit of pointer is used to distinguish between full and empty)
logic[ADDRESS_WIDTH:0] read_pointer, read_pointer_next; // pointer and its next value
logic[ADDRESS_WIDTH:0] read_ptr_gray;
logic[ADDRESS_WIDTH:0] rsync_wp_gray0, rsync_wp_gray; // write pointer to read clock domain sync
logic empty_flag;

//=========================
// read and write pointers
//-------------------------
    fifo_pointer_gray#(.POINTER_WIDTH(ADDRESS_WIDTH+1)) // the + 1 is for distinguishing between full and empty (wrap detection)
    READ_POINTER_GEN (
        .clk(read_clk), 
        .rst_n(rst_n),
        .error_flag(empty_flag), 
        .count_enable(read_enable),
        .pointer(read_pointer),
        .pointer_gray(read_ptr_gray)
    );

    fifo_pointer_gray#(.POINTER_WIDTH(ADDRESS_WIDTH+1)) // the + 1 is for distinguishing between full and empty (wrap detection)
    WRITE_POINTER_GEN (
        .clk(write_clk), 
        .rst_n(rst_n),
        .error_flag(full_flag), 
        .count_enable(write_enable),
        .pointer(write_pointer),
        .pointer_gray(write_ptr_gray)
    );

//=============================
// full and empty detect logic
//-----------------------------
// can get away with non-registered logic for now as gray pointers and their synced versions are registers

    // empty when all bits of read and write pointer match
    always_comb 
    begin
        if (read_ptr_gray == rsync_wp_gray) 
            empty_flag <= 1'b1;
        else 
            empty_flag <= 1'b0;
    end
    always_comb read_empty <= empty_flag;

    // full when address bits of read and write pointer match, but top bits differ
    always_comb
    begin
        if ((write_ptr_gray[ADDRESS_WIDTH] != wsync_rp_gray[ADDRESS_WIDTH]) &&  // indicates write has wrapped more than read
            (write_ptr_gray[ADDRESS_WIDTH-1] != wsync_rp_gray[ADDRESS_WIDTH-1]) &&   // this is an effect of using gray values - 2nd msb must be unequal as gray code reflects around msb
            (write_ptr_gray[ADDRESS_WIDTH-2:0] == wsync_rp_gray[ADDRESS_WIDTH-2:0]))
                full_flag <= 1'b1;
        else
            full_flag <= 1'b0;
    end
    always_comb write_full <= full_flag;

    // synchronise gray version of write pointer to read clock
    always_ff @(posedge read_clk) 
    begin
        rsync_wp_gray <= rsync_wp_gray0;
        rsync_wp_gray0 <= write_ptr_gray;
    end

    // synchronise gray version of read pointer to write clock
    always_ff @(posedge write_clk) 
    begin
        wsync_rp_gray <= wsync_rp_gray0;
        wsync_rp_gray0 <= read_ptr_gray;
    end

    // memory
    assign s_write_enable = write_enable & ~full_flag;
    
    //ram_dp_rw_regout_inferred #(DATA_WIDTH, ADDRESS_WIDTH, RAM_STYLE)
    ram_dp_rw_inferred #(DATA_WIDTH, ADDRESS_WIDTH, RAM_STYLE)
    DATA_MEMORY (
        // write interface
        .write_clk(write_clk),
        .write_data(write_data), 
        .write_enable(s_write_enable),
        .write_address(write_pointer[ADDRESS_WIDTH-1:0]),

        // read interface
        .read_clk(read_clk),
        .read_data(read_data),
        .read_address(read_pointer[ADDRESS_WIDTH-1:0])
    );

endmodule

`endif
