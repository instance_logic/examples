//=============================================================
// pointer with binary and gray outputs for asynchronous fifo
// the pointer will wrap only on (2^POINTER_WIDTH)-1,
// otherwise the gray encoding would be invalid
//-------------------------------------------------------------
`ifndef FIFO_PTR_GRAY_SV
`define FIFO_PTR_GRAY_SV

module fifo_pointer_gray
#(parameter int POINTER_WIDTH=8)
(
    input logic clk,
    input logic rst_n,

    input logic error_flag,  // attach to full or empty, or tie 0 for no block on error
    input logic count_enable,
    output logic[POINTER_WIDTH-1:0] pointer,
    output logic[POINTER_WIDTH-1:0] pointer_gray

);

logic[POINTER_WIDTH-1:0] pointer_next;   // next value for pointer

//=========================
// binary wrapping pointer
//-------------------------
    // pointer register
    always_ff @(posedge clk or negedge rst_n) 
    begin
        if(~rst_n) begin
            pointer <= 0;
        end else begin
            pointer <= pointer_next;
        end
    end

    // pointer incrementer logic
    always_comb 
    begin
        // default no change
        pointer_next <= pointer;    
        
        // increment if a write and fifo no error flag
        if (count_enable & (~error_flag))  
            pointer_next <= pointer + 1;
    end

//=====================
// gray encoded output
//---------------------
    always_ff @(posedge clk)
        pointer_gray <= pointer_next ^ {1'b0, pointer_next[POINTER_WIDTH-1:1]};

endmodule

`endif