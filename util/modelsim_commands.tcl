# ----------------------------------
#  modelsim specific function calls
# ----------------------------------
proc compile_vhdl2008 {lib_name file_name} {
	vcom -work $lib_name -2008 $file_name
    return 1
}

proc compile_sv {lib_name file_name} {
    vlog -sv -work $lib_name $file_name
    return 1
}

proc print_message {message} {
    echo $message
}

proc map_lib {lib_name lib_location} {
	vmap $lib_name $lib_location
}

proc create_lib {lib_name lib_path} {
    vlib "$lib_path$lib_name"
}

proc init_sim {lib_name tb_top} {
    vsim "$lib_name.$tb_top"
    return 1
}

# TODO:
# proc add_wave { wave_list } {
# 	echo $wave_list
# 	# loop through the list and add to waveform
# 	foreach wave_item $wave_list {
# 		echo $wave_item
# 		add wave -position insertpoint $wave_item
# 	}
# }