library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std_unsigned.all;
use std.textio.all;

library osvvm;
use osvvm.RandomPkg.all;

package tb_util_pkg is

    type t_byte_array is array (natural range <>) of std_logic_vector(7 downto 0);

    -- loop until a signal is asserted, or timeout with failure message (single bit)
    procedure p_wait_for_1 (
        signal clk : in std_logic;
        signal input_bit : in std_logic;
        constant K_TIMEOUT : time;
        timeout_message : in string;
        message_severity : in severity_level
        );

    -- loop until a signal is asserted, or timeout with failure message (single bit)
    procedure p_wait_for_1 (
        signal clk : in std_logic;
        signal input_bits : in std_logic_vector;
        constant K_TIMEOUT : time;
        timeout_message : in string;
        message_severity : in severity_level
        );

    -- stores valid bytes to an array from base index
    procedure p_store_valid_bytes(
        data_valid : in std_logic_vector;
        data_bytes : in std_logic_vector;
        base_index : in natural;
        data_store : out t_byte_array;
        constant DISABLE_DEBUG : boolean := TRUE    -- useful to print what bytes were stored
        );

    -- returns how many bits were set in a std_logic_vector
    function get_number_of_1s(input_vector : std_logic_vector) return natural;
    
end package tb_util_pkg;

-----------------------------------------------------------------------------------------------------------------------
-- package body
package body tb_util_pkg is

-----------------------------------
-- waits for '1' on input_bit
    procedure p_wait_for_1 (
        signal clk : in std_logic;
        signal input_bit : in std_logic;
        constant K_TIMEOUT : time;
        timeout_message : in string;
        message_severity : in severity_level
        ) is
    begin
        p_wait_for_1 (
            clk => clk,
            input_bits(0) => input_bit,
            K_TIMEOUT => K_TIMEOUT,
            timeout_message => timeout_message,
            message_severity => message_severity
            );
    end procedure p_wait_for_1;

-----------------------------------
-- waits for any '1' on input_bits
    procedure p_wait_for_1 (
        signal clk : in std_logic;
        signal input_bits : in std_logic_vector;
        constant K_TIMEOUT : time;
        timeout_message : in string;
        message_severity : in severity_level
        ) is

        variable ref_time : time := NOW;
        variable input_bits_detected : boolean;
    begin

        input_bits_detected := FALSE;

        while (input_bits_detected = FALSE) loop
            -- detect any '1'
            for I in 0 to input_bits'high loop
                if (input_bits(I) = '1') then
                    input_bits_detected := TRUE;
                end if;
                -- warn if not 0 or 1
                assert((input_bits(I) = '1') or (input_bits(I) = '0')) 
                report "p_wait_for_1: detected " & to_string(input_bits(I)) & " at bit " & to_string(I) & " while waiting for input_bits!" 
                severity warning;
            end loop;
            -- run timeout procedure
            wait until rising_edge(clk);

            if (NOW - ref_time > K_TIMEOUT) then
                assert(FALSE)
                report timeout_message
                severity message_severity;
            end if;
        end loop;

    end procedure p_wait_for_1;


--------------------------------------------------
-- stores valid bytes to an array from base index
    procedure p_store_valid_bytes(
        data_valid : in std_logic_vector;
        data_bytes : in std_logic_vector;
        base_index : in natural;
        data_store : out t_byte_array;
        constant DISABLE_DEBUG : boolean := TRUE
        ) is

        variable debug_message : line;

    begin

        write(debug_message, "p_store_valid_bytes: " & LF);

        for I in 0 to data_valid'high loop
            if (data_valid(I) = '1') then
                data_store(base_index+I) := data_bytes(I*8+7 downto I*8);

                write(debug_message, "    " & to_string(base_index+I) & " => " & to_hstring(data_bytes(I*8+7 downto I*8)) & LF);
            end if;
        end loop;
        assert(DISABLE_DEBUG)
        report debug_message.all
        severity warning;

        deallocate(debug_message);

    end procedure;

-------------------------------------------------------
-- returns how many bits are '1' in a std_logic_vector
    function get_number_of_1s(input_vector : std_logic_vector) return natural is
    variable ones_count : natural := 0;
    begin
        for I in 0 to input_vector'high loop
            if (input_vector(I) = '1') then
                ones_count := ones_count + 1;
            end if;
        end loop;

        return ones_count;
    end function;
    
end package body;