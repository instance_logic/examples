
import json
import ModelsimCommandGenerator
import os

class ModelsimRunner:

    dir_modelsim = "/media/ericson/82ff8d81-179c-4825-ba39-1de6e34a861d/etc/modelsim_ase/"
    dir_libroot = "~/instance_logic/examples/"
    source_file_list = ""
    deps_lib_list = "../default_dep_libs.json"

    lib_work = "work"
    tb_top = ""

    command_generator = ModelsimCommandGenerator.ModelsimCommandGenerator()

    def __init__(   self,
                    tb_top,
                    sim_file_list,
                    dep_lib_list=deps_lib_list,
                    worklib_name=lib_work,
                    library_root_dir=dir_libroot,
                    modelsim_install_dir=dir_modelsim
            ):

        self.tb_top = tb_top
        self.source_file_list = sim_file_list
        self.deps_lib_list = dep_lib_list
        self.lib_work = worklib_name
        self.dir_modelsim = modelsim_install_dir
        self.dir_libroot = library_root_dir


    def go(self):
        # extract files, libraries, and types from file list
        deps_libs = self.create_lib_list(self.deps_lib_list)
        sources_libs = self.create_lib_list(self.source_file_list)

        # map any system libraries
        self.map_sys_libraries(deps_libs)

        # create and map user libraries from sources and dependencies
        self.create_and_map_source_libs(deps_libs)
        self.create_and_map_source_libs(sources_libs)

        # compile all source files and dependencies to their libraries
        self.compile_files(deps_libs) == 0
        self.compile_files(sources_libs) == 0

        # initialise the simulation
        self.command_generator.init_sim(self.lib_work, self.tb_top)


    def create_and_map_source_libs(self, source_libs):
        libs = source_libs.get('source_libs')

        for lib in libs:

            lib_location = self.dir_libroot + lib

            print(lib_location)

            self.command_generator.create_lib(lib, lib_location)
            self.command_generator.map_lib(lib, lib_location)


    def map_sys_libraries(self, deps_libs):
        system_libs = deps_libs.get('system_libs')

        for sys_lib in system_libs:
            lib_location = self.dir_modelsim + sys_lib

            for sys_lib in system_libs:
                self.command_generator.map_lib(sys_lib, lib_location)


    def create_lib_list(self, spec_file):
        read_file = open(spec_file)
        return json.load(read_file)


    def compile_files(self, sources_libs):
        libs = sources_libs.get('source_libs')
        print(libs)

        for lib in libs:
            num_files = len(libs.get(lib))
            print("\tnumber of files: %d" % num_files)

            for file in range(0, num_files):

                filename = libs.get(lib)[file]
                type = os.path.splitext(libs.get(lib)[file])[1]

                print("\t" + filename)
                print("\t" + lib)
                print("\tcompiling file %d" %file)

                if type == ".sv":
                    assert(self.command_generator.compile_sv(lib, filename) == 0)
                else:
                    assert(self.command_generator.compile_vhdl2008(lib, filename) == 0)
