# ----------------------------------
#  modelsim specific function calls
# ----------------------------------
import os

class ModelsimCommandGenerator:
    SUCCESS = 0

    def __init__(self): pass

    def compile_vhdl2008 (self, lib_name, file_name):
        result = os.system("vcom -work %s -2008 %s" %(lib_name, file_name))
        if result != self.SUCCESS:
            print ("compile_vhdl2008: can't run compile!")

        return result

    def compile_sv (self, lib_name, file_name):
        result = os.system("vlog -sv -work %s %s" %(lib_name, file_name))
        if result != self.SUCCESS:
            print ("compile_sv: can't run compile!")

        return result

    def build_lib_full_path(self, lib_name, lib_path):
        return lib_path + "/"

    def map_lib (self, lib_name, lib_location):
        lib_full_path = self.build_lib_full_path(lib_name, lib_location)
        result = os.system("vmap %s %s" %(lib_name, lib_full_path))

        if result != self.SUCCESS:
            print ("map_lib: can't map library!")

        return result

    def create_lib (self, lib_name, lib_path):
        lib_full_path = self.build_lib_full_path(lib_name, lib_path)

        if not (os.path.exists(lib_full_path+"_info")):
            print("\tCREATING LIBRARY! %s at %s" % (lib_name, lib_full_path))
            result = os.system("vlib %s" % (lib_full_path))

            if result != self.SUCCESS:
                print("\tcreate_lib: can't create library!")

        else:
            result = self.SUCCESS

        return result

    def init_sim (self, lib_name, tb_top):
        result = os.system("vsim %s.%s" %(lib_name, tb_top))

        if result != self.SUCCESS:
            print ("init_sim: can't initialise sim!")

        return result


# TODO:
# proc add_wave { wave_list } {
# 	echo $wave_list
# 	# loop through the list and add to waveform
# 	foreach wave_item $wave_list {
# 		echo $wave_item
# 		add wave -position insertpoint $wave_item
# 	}
# }