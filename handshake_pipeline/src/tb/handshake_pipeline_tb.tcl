# -------------
#  environment
# -------------

# >>>> IMPORTANT!! <<<<<<<<
# change this to point at your modelsim install directory 
set dir_modelsim "/home/ericson/altera/16.0/modelsim_ase/"

# top level tb 
set tb_top "handshake_pipeline_tb"

# waveform macro file name
#set wav_macro_file "wav_multi_header_assembler.do"

# components source directory (use relative path to point at root directory of all libraries)
set dir_libroot "../../../"

# import file with simulator specific function calls
#source "dir_libroot/util/ahdl_commands.tcl"
source "$dir_libroot/util/modelsim_commands.tcl"

# -----------------
#  working library
# -----------------
set lib_work "handshake_pipeline"

# ----------------------
#  dependency libraries
# ----------------------
# osvvm should be in modelsim installation, or get from http://osvvm.org/
map_lib "osvvm" "$dir_modelsim/osvvm/"

# util library
set lib_util "util"
create_lib $lib_util $dir_libroot
map_lib $lib_util "$dir_libroot/$lib_util"

# data streaming library
set lib_data_streaming "data_streaming"
create_lib $lib_data_streaming $dir_libroot
map_lib $lib_data_streaming "$dir_libroot/$lib_data_streaming"

# -------------------------
# waveform macro file path
# -------------------------
#set path_wav_macro "$dir_libroot/$lib_work/src/"

# ------------------------------------------
#  vhdl files and libraries for compilation
# ------------------------------------------
set vhdl_files {}

# UUT
lappend vhdl_files [list $lib_work "$dir_libroot/$lib_work/src/handshake_pipeline_pkg.vhd"]
lappend vhdl_files [list $lib_work "$dir_libroot/$lib_work/src/handshake_pipeline.vhd"]

# simulation dependencies
lappend vhdl_files [list $lib_util "$dir_libroot/$lib_util/src/tb_util_pkg.vhd"]
lappend vhdl_files [list $lib_data_streaming "$dir_libroot/$lib_data_streaming/src/tb/data_streaming_tb_pkg.vhd"]

# test bench
lappend vhdl_files [list $lib_work "$dir_libroot/$lib_work/src/tb/handshake_pipeline_tb.vhd"]

# ---------------
#  compile files
# ---------------
# **TODO: put in a package
proc compile_files { compile_params } {
    set lib_index 0
    set file_index 1
    
    # loop through compile list
    foreach vhdl_file $compile_params {
        
        # extract file name and library name
        set lib_name [lindex $vhdl_file $lib_index]
        set file_name [lindex $vhdl_file $file_index]
        
        # execute compilation 
        if ![compile_vhdl2008 $lib_name $file_name] {
            print_message "compile_files: can't compile\n    $file_name\n into library $lib_name - exiting\n\n"
            return 0 
        }
    }
    return 1
}

# -------
#  main
# -------
# compile files, exit if anything fails
if {[compile_files $vhdl_files]} {
    print_message "TCL: all files compiled successfully!"
} else { 
    print_message "TCL: file compilation failed - terminating script"
    return 0    
}
    
# initialize simulation, exit if anything fails
if {[init_sim $lib_work $tb_top]} {
    print_message "TCL: simulation initialized successfully!"
} else {
    print_message "TCL: can't initialize simulation - aborting script"
    return 0
}

# # add waveform items
# set waveform_macro_file "$path_wav_macro/$wav_macro_file"
# print_message "TCL: adding waveform objects..." $::warning

# if {[execute_macro $waveform_macro_file]} {
#     print_message "TCL: waveform objects added successfully!" $::warning
# } else {
#     print_message "TCL: couldn't add waveform objects - continuing without waveform!" $::error
# }

# add waveform items
# set wave_list {}

# lappend wave_list [list "/frame_buffer_controller_tb/rst"]
# lappend wave_list [list "/frame_buffer_controller_tb/clk"]

# TODO: add_wave wave_list
do wave.do

# # run simulation
# print_message "running simulation!" $::warning
run -all
