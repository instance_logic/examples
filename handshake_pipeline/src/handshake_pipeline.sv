

module handshake_pipeline (

    clk,
        
    // upstream interface
    upst_tvalid,
    upst_tlast,
    upst_tdata,
    upst_tkeep,
    upst_tready,
        
    // downstream interface
    downst_tvalid,
    downst_tlast,
    downst_tdata,
    downst_tkeep,
    downst_tready
);

parameter K_DATA_WIDTH_IN_BYTES = 8;

input clk;
    
// upstream interface
input upst_tvalid;
input upst_tlast;
input[K_DATA_WIDTH_IN_BYTES*8-1:0] upst_tdata;
input[K_DATA_WIDTH_IN_BYTES-1:0] upst_tkeep;
output upst_tready;
        
// downstream interface
output downst_tvalid;
output downst_tlast;
output[K_DATA_WIDTH_IN_BYTES*8-1:0] downst_tdata;
output[K_DATA_WIDTH_IN_BYTES-1:0] downst_tkeep;
input downst_tready;

// registered version of ready input
logic r_tready;

// output registers
logic r_downst_tvalid;
logic r_downst_tlast;
logic[K_DATA_WIDTH_IN_BYTES*8-1:0] r_downst_tdata;
logic[K_DATA_WIDTH_IN_BYTES-1:0] r_downst_tkeep;

// pipeline signals
logic[1:0][K_DATA_WIDTH_IN_BYTES-1:0] r_keep_pipe;
logic[1:0] r_tvalid;
logic[1:0] r_tlast;
logic[1:0][K_DATA_WIDTH_IN_BYTES*8-1:0] r_data_pipe;
											   							  
    assign upst_tready = r_tready;
    
    always_ff @(posedge clk) begin : CONTROL_PIPELINE

        // register tready input
        r_tready <= downst_tready;
            
        // control out delayed by different amounts 
        // depending on wait state determined by downstream tready
        if (downst_tready == 1'b1) begin
            
            // coming out of wait state, use old data
            if (r_tready == 1'b0) begin
                r_downst_tkeep <= r_keep_pipe[1];
                r_downst_tvalid <= r_tvalid[1];
                r_downst_tlast <= r_tlast[1];
            
            // normal operation, use latest data
            end else begin
                r_downst_tkeep <= r_keep_pipe[0];
                r_downst_tvalid <= r_tvalid[0];
                r_downst_tlast <= r_tlast[0];
            end
            
        // wait state
        end else begin
            // going into wait state, use old data, so output doesn't change
            if (r_tready == 1'b1) begin
                r_downst_tkeep <= r_keep_pipe[1];
                r_downst_tvalid <= r_tvalid[1];
                r_downst_tlast <= r_tlast[1];
            end
        end             
            
        // control input pipeline shifting left
        if (r_tready == 1'b1) begin
            r_tlast <= {r_tlast[0], upst_tlast};
            r_keep_pipe <= {r_keep_pipe[0], upst_tkeep};
            r_tvalid <= {r_tvalid[0], upst_tvalid};
        end
            
    end : CONTROL_PIPELINE

    assign downst_tkeep = r_downst_tkeep;
    assign downst_tlast = r_downst_tlast;
    assign downst_tvalid = r_downst_tvalid;
    
    always_ff @(posedge clk) begin : DATA_PIPELINE
            
        // data out delayed by different amounts depending on downstream ready 
        // normal state puts latest data out
        if (downst_tready == 1'b1) begin    
            
            // coming out of paused state puts old data out
            if (r_tready == 1'b0) begin
                r_downst_tdata <= r_data_pipe[1];
            
            // normal operation, use latest data
            end else begin
                r_downst_tdata <= r_data_pipe[0];
            end

        end else if (r_tready == 1'b1) begin
            r_downst_tdata <= r_data_pipe[1];
        end

        // input pipeline validated by registered tready
        if (r_tready == 1'b1) begin
            r_data_pipe[1] <= {r_data_pipe[0], upst_tdata};
        end
    end : DATA_PIPELINE

    assign downst_tdata = r_downst_tdata;
    
endmodule // handshake_pipeline
