

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity handshake_pipeline is
    generic(
        K_DATA_WIDTH_IN_BYTES : integer := 8;
        K_USER_WIDTH_IN_BITS : integer := 8
    );
    port (
        clk : in std_logic;	
        
        -- upstream interface
        upst_tvalid : in std_logic;
        upst_tlast : in std_logic;
        upst_tdata : in std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);
        upst_tkeep : in std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0);
        upst_tready : out std_logic;
        
        -- downstream interface
        downst_tvalid : out std_logic;
        downst_tlast : out std_logic;
        downst_tdata : out std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);
        downst_tkeep : out std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0);
        downst_tready : in std_logic 
    );
end entity handshake_pipeline;

architecture handshake_pipeline of handshake_pipeline is 
    	
	signal r_tready : std_logic;
    
begin		
										   							  
    upst_tready <= r_tready;
    
    P_CONTROL_PIPELINE : process(clk)
        type t_keepPipe is array(1 downto 0) of std_logic_vector(upst_tkeep'range);
        variable r_keep_pipe : t_keepPipe;
        
        variable r_tvalid : std_logic_vector(1 downto 0);
        variable r_tlast : std_logic_Vector(1 downto 0);
        
    begin
        if (rising_edge(clk)) then
        -- TREADY IN --------
            r_tready <= downst_tready;
            
        -- CONTROL OUT --------
            -- control out delayed by different amounts depending on downstream ready 
            if (downst_tready = '1') then
                
                -- coming out of wait state, use old data
                if (r_tready = '0') then
                    downst_tkeep <= r_keep_pipe(1);
                    downst_tvalid <= r_tvalid(1);
                    downst_tlast <= r_tlast(1);
                
                -- normal operation, use latest data
                else
                    downst_tkeep <= r_keep_pipe(0);
                    downst_tvalid <= r_tvalid(0);
                    downst_tlast <= r_tlast(0);
                end if;
            
            -- wait state
            else
                -- going into wait state, use old data (so no change)
                if (r_tready = '1') then
                    downst_tkeep <= r_keep_pipe(1);
                    downst_tvalid <= r_tvalid(1);
                    downst_tlast <= r_tlast(1);
                end if;
            end if;             
            
            -- control input pipeline
            if (r_tready = '1') then
                r_tlast := r_tlast(0) & upst_tlast;
                r_keep_pipe := r_keep_pipe(0) & upst_tkeep;
                r_tvalid := r_tvalid(0) & upst_tvalid;
            end if;
            
        end if;
    end process P_CONTROL_PIPELINE;
    
    
    P_DATA_PIPELINE : process(clk)
        type t_dataPipe is array(1 downto 0) of std_logic_vector(upst_tdata'range);
        variable r_data_pipe : t_dataPipe;

    begin
        if (rising_edge(clk)) then
            
        -- DATA OUT --------
            -- data out delayed by different amounts depending on downstream ready 
            if (downst_tready = '1') then    -- normal state puts latest data out
                
                -- coming out of paused state puts old data out
                if (r_tready = '0') then
                    downst_tdata <= r_data_pipe(1);
                    downst_tuser <= r_userPipe(1);
                
                -- normal operation, use latest data
                else
                    downst_tdata <= r_data_pipe(0);
                end if;
            else
                if (r_tready = '1') then
                    downst_tdata <= r_data_pipe(1);
                end if;
            end if;
            
        -- PIPELINE --------
            -- input pipeline validated by registered tready
            if (r_tready = '1') then
                r_data_pipe(1) := r_data_pipe(0);
                r_userPipe(1) := r_userPipe(0);
                
                r_data_pipe(0) := upst_tdata;
                r_userPipe(0) := upst_tuser;
            end if;
        end if;
    end process P_DATA_PIPELINE;
    
end architecture handshake_pipeline;
