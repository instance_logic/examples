onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /frame_buffer_controller_tb/clk
add wave -noupdate /frame_buffer_controller_tb/rst
add wave -noupdate -expand -group write_interface /frame_buffer_controller_tb/write_tvalid
add wave -noupdate -expand -group write_interface /frame_buffer_controller_tb/write_tlast
add wave -noupdate -expand -group write_interface -radix hexadecimal /frame_buffer_controller_tb/write_tdata
add wave -noupdate -expand -group write_interface -radix hexadecimal /frame_buffer_controller_tb/write_tkeep
add wave -noupdate -expand -group write_interface /frame_buffer_controller_tb/write_tready
add wave -noupdate -expand -group write_interface -radix hexadecimal /frame_buffer_controller_tb/U_UUT/s_write_address
add wave -noupdate -expand -group read_interface /frame_buffer_controller_tb/read_tvalid
add wave -noupdate -expand -group read_interface /frame_buffer_controller_tb/read_tlast
add wave -noupdate -expand -group read_interface -radix hexadecimal /frame_buffer_controller_tb/read_tdata
add wave -noupdate -expand -group read_interface -radix hexadecimal /frame_buffer_controller_tb/read_tkeep
add wave -noupdate -expand -group read_interface /frame_buffer_controller_tb/read_tready
add wave -noupdate -expand -group read_interface /frame_buffer_controller_tb/U_UUT/s_empty
add wave -noupdate -expand -group read_interface /frame_buffer_controller_tb/U_UUT/r_read_state
add wave -noupdate -expand -group read_interface -radix hexadecimal /frame_buffer_controller_tb/U_UUT/s_read_address
add wave -noupdate /frame_buffer_controller_tb/overflow
add wave -noupdate /frame_buffer_controller_tb/underflow
add wave -noupdate /frame_buffer_controller_tb/enable_reader
add wave -noupdate /frame_buffer_controller_tb/enable_writer
add wave -noupdate /frame_buffer_controller_tb/frames_written
add wave -noupdate /frame_buffer_controller_tb/frames_read
add wave -noupdate /frame_buffer_controller_tb/frame_diff
add wave -noupdate /frame_buffer_controller_tb/P_EVENT_SCHEDULER/ref_num_frames
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {39656453 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 578
configure wave -valuecolwidth 55
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {137913526 ps}
