-----------------------------------------------------------------------------------------------------------------------
-- testbench for frame buffer module
--
-- continually writes frames to the frame buffer, and reads them back
-- 
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

library osvvm;
use osvvm.RandomPkg.all;

library data_streaming;
use data_streaming.data_streaming_tb_pkg.all;

library util;
use util.tb_util_pkg.all;

use work.frame_buffer_package.all;

entity frame_buffer_controller_tb is
end entity frame_buffer_controller_tb;

architecture tb of frame_buffer_controller_tb is
  
    -- simulation parameters
    constant K_NUM_WRITE_FRAMES : natural range 16 to natural'high := 512;
    constant K_MIN_FRAME_BYTES : natural := 1;

    -- UUT parameters
    constant K_NUM_BUFFER_ELEMENTS : natural range 4 to natural'high := 64;
    constant K_MAX_FRAME_BYTES : natural range 1 to natural'high := 64;
    constant K_DATA_WIDTH_IN_BYTES : natural := 4;

    -- clocks and reset
    constant K_CLOCK_PERIOD : time := 10 ns;
    constant K_RESET_CLOCKS : integer := 4;

    signal clk: std_logic := '1';
    signal rst: std_logic;

    -- subtype to define read and write frame objects
    subtype t_data_frame is util.tb_util_pkg.t_byte_array(K_MAX_FRAME_BYTES-1 downto 0);

    -- frame source stimulae
    signal write_tvalid: std_logic;
    signal write_tlast: std_logic;
    signal write_tdata: std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);
    signal write_tkeep: std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0);
    signal write_tready: std_logic;
    
    -- frame processor stimulae
    signal read_tvalid: std_logic;
    signal read_tlast: std_logic;
    signal read_tdata: std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);
    signal read_tkeep: std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0);
    signal read_tready: std_logic;

    -- event scheduling
    signal frames_written : integer;
    signal frames_read : integer;
    signal frame_diff : integer;
    signal enable_writer : boolean;
    signal enable_reader : boolean;
    signal overflow, noverflow : std_logic;
    signal underflow, nunderflow : std_logic;

begin
 
-----------------------------------------------------------------------------------------------------------------------
-- EVENT SCHEDULER
    P_EVENT_SCHEDULER : process
        constant K_OVERFLOW_FAIL_MESSAGE : string := "P_EVENT_SCHEDULER: UUT should have gone into overflow by now!";
        constant K_NOVERFLOW_FAIL_MESSAGE : string := "P_EVENT_SCHEDULER: UUT should have exited overflow condition by now!";
        constant K_UNDERFLOW_FAIL_MESSAGE : string := "P_EVENT_SCHEDULER: UUT should have gone into underflow by now!";
        constant K_NUNDERFLOW_FAIL_MESSAGE : string := "P_EVENT_SCHEDULER: UUT should have exited underflow condition by now!";
        constant K_OUFLOW_TIMEOUT : time := 100 us;
        variable ref_num_frames : integer;
    begin
        while (rst /= '0') loop
            enable_reader <= FALSE;
            enable_writer <= FALSE;
            ref_num_frames := 0;
            wait until rising_edge(clk);    
        end loop ; -- identifier

        -- run normally for the first ~25% of the frames (integer division is ok, doesn't have to be precise)
        report "EVENT_SCHEDULER: running normally for " & to_string(K_NUM_WRITE_FRAMES/4) & " frames..." severity warning;

        enable_reader <= TRUE;
        enable_writer <= TRUE;
        while (frames_written < K_NUM_WRITE_FRAMES/4) loop
            wait until rising_edge(clk);
        end loop;

        -- cause overflow
        report "EVENT_SCHEDULER: attempting to cause overflow condition by disabling reader..." severity warning;

        enable_reader <= FALSE;
        enable_writer <= TRUE;
        p_wait_for_1 (clk, overflow, K_OUFLOW_TIMEOUT, K_OVERFLOW_FAIL_MESSAGE, failure);

        report "EVENT_SCHEDULER: detected overflow condition!" severity warning;
        ref_num_frames := frames_written;   -- store number of frames so far

        -- make overflow condition go away
        report "EVENT_SCHEDULER: making overflow condition go away by enabling read and disabling write..." severity warning;
        
        enable_reader <= TRUE;
        enable_writer <= FALSE;
        -- make overflow condition go away  
        p_wait_for_1 (clk, noverflow, K_OUFLOW_TIMEOUT, K_NOVERFLOW_FAIL_MESSAGE, failure);

        report "EVENT_SCHEDULER: overflow condition gone away!" severity warning;
        
        -- run normally for another 8 frames
        report "EVENT_SCHEDULER: running normally for " & to_string(K_NUM_WRITE_FRAMES/4) & " frames..." severity warning;

        enable_reader <= TRUE;
        enable_writer <= TRUE;
        ref_num_frames := frames_written;
        while ((frames_written - ref_num_frames) < 8) loop
            wait until rising_edge(clk);
        end loop;

        -- cause underflow
        report "EVENT_SCHEDULER: attempting to cause underflow condition by disabling writer..." severity warning;

        enable_reader <= TRUE;
        enable_writer <= FALSE;
        p_wait_for_1 (clk, underflow, K_OUFLOW_TIMEOUT, K_UNDERFLOW_FAIL_MESSAGE, failure);

        -- make underflow condition go away
        report "EVENT_SCHEDULER: making underflow condition go away by enabling write and disabling read..." severity warning;
        
        enable_reader <= FALSE;
        enable_writer <= TRUE;
        p_wait_for_1 (clk, nunderflow, K_OUFLOW_TIMEOUT, K_NUNDERFLOW_FAIL_MESSAGE, failure);

        report "EVENT_SCHEDULER: overflow condition gone away!" severity warning;

        -- run normally for remainder of frames
        report "EVENT_SCHEDULER: running normally for remaining frames..." severity warning;

        enable_reader <= TRUE;
        enable_writer <= TRUE;
        while (frames_written < K_NUM_WRITE_FRAMES) loop
            wait until rising_edge(clk);
        end loop;

        -- done
        assert(FALSE) report LF & LF & "    >>>> simulation done! <<<<" & LF severity failure;

        wait;

    end process P_EVENT_SCHEDULER;

    noverflow <= not(overflow);
    nunderflow <= not(underflow);
    frame_diff <= frames_written - frames_read;

-----------------------------------------------------------------------------------------------------------------------
-- WRITE
    P_WRITE_GENERATOR : process
        variable rand_generator : osvvm.RandomPkg.RandomPType;
        variable frame_size : natural;
        variable frame_data : t_data_frame;
    begin
        -- wait for reset to go away
        while (rst /= '0') loop
            rand_generator.initSeed(0);
            write_tvalid <= '0';
            write_tlast <= '0';
            write_tdata <= (others => '0');
            write_tkeep <= (others => '0');
            frames_written <= 0;
            wait until rising_edge(clk);
        end loop;

        -- generate random frames
        while (enable_writer) loop
            -- zeroize frame data
            frame_data := (others => X"00");

            -- generate random size
            frame_size := rand_generator.randInt(K_MIN_FRAME_BYTES, K_MAX_FRAME_BYTES);  

            -- generate frame with random data for that size
            for J in 0 to frame_size-1 loop
                frame_data(J) := rand_generator.RandSlv(8);
            end loop;

            -- write the frame to the buffer
            p_send_frame_data (
                clk, frame_data(frame_size-1 downto 0), 
                write_tvalid, write_tlast, write_tdata, write_tkeep ,write_tready, 1, 1 us
                );

            frames_written <= frames_written + 1;

            wait until rising_edge(clk);

        end loop;

        -- if not enabled, do nothing for a clock
        wait until rising_edge(clk);

    end process P_WRITE_GENERATOR;

-----------------------------------------------------------------------------------------------------------------------
-- READ
    P_READ_GENERATOR : process
        variable rand_generator : osvvm.RandomPkg.RandomPType;
        variable read_frame : t_data_frame;
    begin
        -- wait for reset to go away
        while (rst /= '0') loop
            rand_generator.initSeed(0);
            read_tready <= '0';
            frames_read <= 0;
            wait until rising_edge(clk);
        end loop;

        while (enable_reader) loop 
            -- get frame data from read interface
            p_get_frame_data(
                clk, read_tvalid, read_tdata, read_tkeep, read_tlast, read_tready,
                10 us, "UUT hasn't sent data for 10 us!", failure, read_frame);

            frames_read <= frames_read + 1;

            wait until rising_edge(clk);
        end loop;

        -- when not enabled, do nothing for a clock
        wait until rising_edge(clk);

    end process P_READ_GENERATOR;

-----------------------------------------------------------------------------------------------------------------------
-- UUT INSTANTIATION
    U_UUT : frame_buffer_controller 
    generic map ( 
        K_NUM_BUFFER_ELEMENTS => K_NUM_BUFFER_ELEMENTS,
        K_MAX_FRAME_BYTES => K_MAX_FRAME_BYTES,
        K_DATA_WIDTH_IN_BYTES => K_DATA_WIDTH_IN_BYTES 
       )
    port map ( 
        clk => clk,
        rst => rst,
        write_tvalid => write_tvalid,
        write_tlast => write_tlast,
        write_tdata => write_tdata,
        write_tkeep => write_tkeep,
        write_tready => write_tready,
        write_overflow => overflow,
        read_tvalid => read_tvalid,
        read_tlast => read_tlast,
        read_tdata => read_tdata,
        read_tkeep => read_tkeep,
        read_tready => read_tready,
        read_underflow => underflow
        );

-----------------------------------------------------------------------------------------------------------------------
-- CLOCKS AND RESET
    clk <= not(clk) after K_CLOCK_PERIOD / 2;
    rst <= '1', '0' after K_CLOCK_PERIOD * 4;

end tb;