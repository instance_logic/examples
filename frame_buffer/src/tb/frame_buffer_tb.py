import sys

sys.path.insert(0, '../../../util/sim_script')

import ModelsimRunner

sim_runner = ModelsimRunner.ModelsimRunner(tb_top='frame_buffer_controller_tb',
                                           sim_file_list='sim_file_lib.json',
                                           dep_lib_list='sim_dep_lib.json',
                                           worklib_name='frame_buffer',
                                           library_root_dir='../../../')

sim_runner.go()
