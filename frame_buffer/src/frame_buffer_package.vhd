library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

package frame_buffer_package is

    function f_calc_location_width(max_frame_bytes, data_width_in_bytes : natural) return integer;

    component frame_buffer_controller is
        generic(
            K_NUM_BUFFER_ELEMENTS : integer range 2 to integer'high := 4;
            K_MAX_FRAME_BYTES : integer range 2 to integer'high := 2048;
            K_DATA_WIDTH_IN_BYTES : integer range 1 to integer'high := 8    -- width in bytes of storage data
        );
        port (            
            -- for simplicity, read and write interfaces are on the same clock
            -- for asynchronous operation, choose the fastest clock of the read and write domains
            -- as 'clk' input, and synchronise the interface accordingly
            clk : in std_logic;
            rst : in std_logic;
            
            -- upstream interface to frame generator
            write_tvalid : in std_logic; -- write data valid
            write_tlast : in std_logic;  -- indicates last beat of write data
            write_tdata : in std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);   -- write data
            write_tkeep : in std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0); -- indicates which write bytes contain data
            write_tready : out std_logic;    -- output indicates this unit is ready to receive data
            write_overflow : out std_logic; -- flags overflow condition
            
            -- downstream interface to frame processor
            read_tvalid : out std_logic;   -- output data valid ('1' when there is some data in the buffer)
            read_tlast : out std_logic;  -- indicates last beat of read data
            read_tdata : out std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);    -- read data 
            read_tkeep : out std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0);   -- indicates which input bytes contain data
            read_tready : in std_logic;  -- indicates the current output data has been consumed by the frame processor   
            read_underflow : out std_logic  -- flags underflow condition   
        );
    end component frame_buffer_controller;

    component location_pointer is
        generic(
            K_LOCATION_SEL_WIDTH : integer
        );
        port (
            
            clk : in std_logic;
            rst : in std_logic;
            
            -- interface to read / write controls
            tvalid : in std_logic; -- data valid
            tlast : in std_logic;  -- indicates last beat of write data
            tready : in std_logic;    -- indicates data has been consumed

            -- location pointer
            location : out std_logic_vector(K_LOCATION_SEL_WIDTH-1 downto 0)

        );
    end component location_pointer;

    component element_pointer_lose_new is
        generic(
            K_ELEMENT_SEL_WIDTH : integer := 2
        );
        port (
            
            clk : in std_logic;
            rst : in std_logic;
            
            -- interface to read / write controls
            tvalid : in std_logic; -- write data valid
            tlast : in std_logic;  -- indicates last beat of write data
            tready : in std_logic;    -- indicates data has been consumed
            
            -- opposite element pointer 
            -- (if this entity is a read element manager, input the write element here, and vice versa)
            element_opposite : in std_logic_vector(K_ELEMENT_SEL_WIDTH-1 downto 0);

            -- element pointer
            element_pointer : out std_logic_vector(K_ELEMENT_SEL_WIDTH-1 downto 0);
            over_under_flow : out std_logic

        );
    end component element_pointer_lose_new;

end package frame_buffer_package;

package body frame_buffer_package is

    function f_calc_location_width(max_frame_bytes, data_width_in_bytes : natural) return integer is
        constant K_MAX_WORDS_PER_FRAME : real := (real(max_frame_bytes)) / (real(data_width_in_bytes));
    begin    	
        -- smaller than 1 word doesn't make sense
        if (K_MAX_WORDS_PER_FRAME < 1.0) then 
            assert(FALSE) 
            report "frame_buffer_controller.f_calc_location_width: can't calculate location width for < 1 words per frame"
            severity failure;
            return 0;
        -- 1 word would cause the log2 function to return 0 for the width, so return 1 instead
        elsif (K_MAX_WORDS_PER_FRAME = 1.0) then 
            return 1;        
        -- any other number of words, use the log2 function to calculate the width, and round up
        else
            return integer(ceil(log2(K_MAX_WORDS_PER_FRAME)));
        end if;

   	end function f_calc_location_width;

end package body frame_buffer_package;