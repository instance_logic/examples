library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity location_pointer is
    generic(
        K_LOCATION_SEL_WIDTH : integer
    );
    port (
        
        clk : in std_logic;
        rst : in std_logic;
        
        -- interface to read / write controls
        tvalid : in std_logic; -- data valid
        tlast : in std_logic;  -- indicates last beat of write data
        tready : in std_logic;    -- indicates data has been consumed

        -- location pointer
        location : out std_logic_vector(K_LOCATION_SEL_WIDTH-1 downto 0)

    );
end entity location_pointer;

architecture rtl of location_pointer is

    signal r_location : std_logic_vector(location'range);

begin

    P_LOCATION : process(clk)
    begin
        if (rising_edge(clk)) then
            -- synchronous reset
            if (rst = '1') then
                r_location <= (others => '0');
            
            -- pointer always does something when data is consumed
            elsif ((tvalid = '1') and (tready = '1')) then

                -- clear location at end of frame
                if (tlast = '1') then
                    r_location <= (others => '0');
                
                -- otherwise, increment (no need to protect against going over max frame size)
                else
                    r_location <= r_location + 1;
                end if;
            end if;
        end if;
    end process P_LOCATION; 

    location <= r_location;

end architecture rtl;
