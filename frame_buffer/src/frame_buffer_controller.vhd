----------------
-- frame buffer
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.math_real.all;

use work.frame_buffer_package.all;

entity frame_buffer_controller is
    generic(
    	K_NUM_BUFFER_ELEMENTS : integer range 2 to integer'high := 4;
    	K_MAX_FRAME_BYTES : integer range 2 to integer'high := 2048;
        K_DATA_WIDTH_IN_BYTES : integer range 1 to integer'high := 8    -- width in bytes of storage data
    );
    port (
        
        -- for simplicity, read and write interfaces are on the same clock
        -- for asynchronous operation, choose the fastest clock of the read and write domains
        -- as 'clk' input, and synchronise the interface accordingly
        clk : in std_logic;
        rst : in std_logic;
        
        -- upstream interface to frame generator
        write_tvalid : in std_logic; -- write data valid
        write_tlast : in std_logic;  -- indicates last beat of write data
        write_tdata : in std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);   -- write data
        write_tkeep : in std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0); -- indicates which write bytes contain data
        write_tready : out std_logic;    -- output indicates this unit is ready to receive data
        write_overflow : out std_logic;  -- flags overflow condition 
        
        -- downstream interface to frame processor
        read_tvalid : out std_logic;   -- output data valid ('1' when there is some data in the buffer)
        read_tlast : out std_logic;  -- indicates last beat of read data
        read_tdata : out std_logic_vector(K_DATA_WIDTH_IN_BYTES*8-1 downto 0);    -- read data 
        read_tkeep : out std_logic_vector(K_DATA_WIDTH_IN_BYTES-1 downto 0);   -- indicates which input bytes contain data
        read_tready : in std_logic;	-- indicates the current output data has been consumed by the frame processor
        read_underflow : out std_logic  -- flags underflow condition 
  
    );
end entity frame_buffer_controller;

architecture RTL of frame_buffer_controller is
    
    constant K_ELEMENT_SEL_WIDTH : natural := integer(ceil(log2(real(K_NUM_BUFFER_ELEMENTS))));
    constant K_LOCATION_SEL_WIDTH : natural := f_calc_location_width(K_MAX_FRAME_BYTES, K_DATA_WIDTH_IN_BYTES);

-- bit locations to store data and control in memory
    constant K_MEM_BIT_DATA_LO : integer := 0;
    constant K_MEM_BIT_DATA_HI : integer := write_tdata'high;
    constant K_MEM_BIT_KEEP_LO : integer := K_MEM_BIT_DATA_HI + 1;
    constant K_MEM_BIT_KEEP_HI : integer := K_MEM_BIT_KEEP_LO + write_tkeep'high;
    constant K_MEM_BIT_LAST : integer := K_MEM_BIT_KEEP_HI + 1;
    
-- write control
    signal s_write_element : std_logic_vector(K_ELEMENT_SEL_WIDTH-1 downto 0);
    signal s_write_location : std_logic_vector(K_LOCATION_SEL_WIDTH-1 downto 0);

-- read control    
    signal s_read_element : std_logic_vector(K_ELEMENT_SEL_WIDTH-1 downto 0);
    signal s_read_location : std_logic_vector(K_LOCATION_SEL_WIDTH-1 downto 0);
    signal s_read_tlast : std_logic;
    signal s_empty : std_logic;

    signal r_read_state : std_logic;

-- memory
    constant K_MEM_ADDRESS_WIDTH : integer := K_ELEMENT_SEL_WIDTH + K_LOCATION_SEL_WIDTH;
    -- data width in memory includes tkeep and tlast
    constant K_MEM_DATA_WIDTH : integer := write_tdata'length + write_tkeep'length + 1;
    
    type t_memory is array(2**K_MEM_ADDRESS_WIDTH-1 downto 0) of std_logic_vector(K_MEM_DATA_WIDTH-1 downto 0);
    shared variable buffer_memory : t_memory;   -- shared variable is less memory intensive on the simulator

    signal s_read_address, s_write_address : std_logic_vector(K_MEM_ADDRESS_WIDTH-1 downto 0);

begin

-----------------------------------------------------------------------------------------------------------------------
-- WRITE
-----------------------------------------------------------------------------------------------------------------------
    -- write ready fixed '1' because we're managing write overflow conditions using the frame buffer
    write_tready <= '1';
 
    -- write buffer element pointer increments at the end of each frame write...
    -- ...unless there's an overflow condition, in which case the pointer doesn't...
    -- ...increment, thus causing the next frame to overwrite the latest written frame
    U_WRITE_ELEMENT_POINTER : element_pointer_lose_new 
    generic map(
        K_ELEMENT_SEL_WIDTH => K_ELEMENT_SEL_WIDTH
    )
    port map (
        
        clk => clk,
        rst => rst,
        
        tvalid => write_tvalid,
        tlast  => write_tlast,
        tready => '1',  -- always ready
        
        element_opposite => s_read_element,
        element_pointer => s_write_element,
        over_under_flow => write_overflow

    );

    -- write location pointer indicates which address in the current write buffer element...
    -- ...the next word will be written to
    U_WRITE_LOCATION_POINTER : location_pointer
    generic map (
        K_LOCATION_SEL_WIDTH => K_LOCATION_SEL_WIDTH
    )
    port map (
        
        clk => clk,
        rst => rst,
        
        tvalid => write_tvalid,
        tlast => write_tlast,
        tready => '1',

        location => s_write_location
    );

-----------------------------------------------------------------------------------------------------------------------
-- READ
-----------------------------------------------------------------------------------------------------------------------
    U_READ_ELEMENT_POINTER : element_pointer_lose_new 
    generic map(
        K_ELEMENT_SEL_WIDTH => K_ELEMENT_SEL_WIDTH
    )
    port map (
        
        clk => clk,
        rst => rst,
        
        tvalid => r_read_state,
        tlast  => s_read_tlast,
        tready => read_tready,
        
        element_opposite => s_write_element,
        element_pointer => s_read_element,
        over_under_flow => read_underflow
    );

    -- read location pointer indicates which address in the current read buffer element...
    -- ...the next word will be read from
    U_READ_LOCATION_POINTER : location_pointer
    generic map (
        K_LOCATION_SEL_WIDTH => K_LOCATION_SEL_WIDTH
    )
    port map (
        
        clk => clk,
        rst => rst,
        
        -- interface to read / write controls
        tvalid => r_read_state,
        tlast => s_read_tlast,
        tready => read_tready,

        -- location pointer
        location => s_read_location
    );

    -- empty detection logic
    s_empty <= '1' when s_read_element = s_write_element else '0';

    -- read state indicator
    P_READ_STATE : process(clk)
    begin
        if (rising_edge(clk)) then
            if (rst= '1') then
                r_read_state <= '0';
            else
                -- not currently reading
                if (r_read_state = '0') then
                    -- start reading only if there's data
                    r_read_state <= not(s_empty);
                
                -- currently reading
                else
                    -- stop reading when the frame is fully read and data has been consumed
                    if ((s_read_tlast = '1') and (read_tready = '1')) then
                        r_read_state <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process P_READ_STATE;

    -- read valid when there's data
    read_tvalid <= r_read_state;

-----------------------------------------------------------------------------------------------------------------------
-- MEMORY
-----------------------------------------------------------------------------------------------------------------------
    -- full read and write addresses are element and location concatenated
    s_read_address <= s_read_element & s_read_location;
    s_write_address <= s_write_element & s_write_location;

    -- memory write
    P_BUFFER_MEM_WRITE : process(clk)
    begin
        if rising_edge(clk) then
            -- write always happens - the write element pointer handles overflows
            if (write_tvalid = '1') then
                -- tkeep and tlast stored with the data to demarcate the valid frame bytes
                buffer_memory(to_integer(unsigned(s_write_address)))(K_MEM_BIT_DATA_HI downto K_MEM_BIT_DATA_LO) := write_tdata;
                buffer_memory(to_integer(unsigned(s_write_address)))(K_MEM_BIT_LAST) := write_tlast;
                buffer_memory(to_integer(unsigned(s_write_address)))(K_MEM_BIT_KEEP_HI downto K_MEM_BIT_KEEP_LO) := write_tkeep;

            end if;
        end if;
    end process P_BUFFER_MEM_WRITE;

    -- memory read
    read_tdata <= buffer_memory( to_integer(unsigned(s_read_address)) )(K_MEM_BIT_DATA_HI downto K_MEM_BIT_DATA_LO);
    s_read_tlast <= buffer_memory( to_integer(unsigned(s_read_address)) )(K_MEM_BIT_LAST);
    read_tlast <= s_read_tlast;
    read_tkeep <= buffer_memory( to_integer(unsigned(s_read_address)) )(K_MEM_BIT_KEEP_HI downto K_MEM_BIT_KEEP_LO);

end architecture RTL;
