library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity element_pointer_lose_new is
    generic(
    	K_ELEMENT_SEL_WIDTH : integer := 2
    );
    port (
        
        clk : in std_logic;
        rst : in std_logic;
        
        -- interface to read / write controls
        tvalid : in std_logic; -- write data valid
        tlast : in std_logic;  -- indicates last beat of write data
        tready : in std_logic;    -- indicates data has been consumed
        
        -- opposite element pointer 
        -- (if this entity is a read element manager, input the write element here, and vice versa)
        element_opposite : in std_logic_vector(K_ELEMENT_SEL_WIDTH-1 downto 0);

        -- element pointer
        element_pointer : out std_logic_vector(K_ELEMENT_SEL_WIDTH-1 downto 0);
        over_under_flow : out std_logic

    );
end entity element_pointer_lose_new;

architecture rtl of element_pointer_lose_new is

    signal r_element : std_logic_vector(K_ELEMENT_SEL_WIDTH-1 downto 0);

begin

    P_ELEMENT_POINTER : process(clk)
        -- this variable stays at r_element + 1
        -- it's used for better timing when checking for overflow (r_element + 1 = element_opposite) 
        variable rv_next_element : std_logic_vector(r_element'range);
    begin
        if (rising_edge(clk)) then
            -- synchronous reset
            if (rst = '1') then
                -- current element reset to 0
                r_element <= (others => '0');
                -- next element reset to 1
                rv_next_element := (others => '0');
                rv_next_element(0) := '1';
                over_under_flow <= '0';
            
            -- time for element to increment at end of frame
            elsif (tvalid = '1') and (tlast = '1') then

                -- if no overflow / underflow, increment element otherwise don't increment. 
                -- in the write case, not incrementing causes the next write frame to overwrite the current one
                -- in the read case, it causes the last read frame to be repeated
                if (rv_next_element /= element_opposite) then
                    r_element <= r_element + 1;
                    rv_next_element := rv_next_element + 1;
                else
                    over_under_flow <= '1';
                end if;

            end if;

            -- clear over or underflow when condition no longer exists
            if (over_under_flow = '1') then

                if (rv_next_element /= element_opposite) then
                    over_under_flow <= '0';
                end if;

            end if;

        end if;
    end process P_ELEMENT_POINTER;

    element_pointer <= r_element;

end rtl;