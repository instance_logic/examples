
module local_reset (
// Asynchronous reset active high
	input rst,

// local synchronous reset 
	input clk, 
	output local_rst
);

logic[1:0] reset_bridge;

	always_ff @(posedge clk or posedge rst) begin
		if(rst) begin
			reset_bridge <= 2'b11;
		end else begin
			reset_bridge <= {1'b0, reset_bridge[1]};
		end
	end

	assign local_rst = reset_bridge[0];

endmodule